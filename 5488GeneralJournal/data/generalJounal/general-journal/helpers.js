Handlebars.registerHelper('ifEq', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

function getPageNumber (pageIndex) {
    if (pageIndex == null) {
        return ''
    }
    
    const pageNumber = pageIndex + 1
    
    return pageNumber
}

function getTotalPages (pages) {
    if (!pages) {
        return ''
    }
    
    return pages.length
}

// 2 decimal

function total(items) {
    var sum = 0
    items.forEach(function (i) {
        sum += i.bedrag
    })
    return sum
}

function numberFormat (value, options) {
    // Helper parameters
    var dl = options.hash['decimalLength'] || 2;
    var ts = options.hash['thousandsSep'] || ',';
    var ds = options.hash['decimalSep'] || '.';

    // Parse to float
    var newValue = parseFloat(value);

    // The regex
    var re = '\\d(?=(\\d{3})+' + (dl > 0 ? '\\D' : '$') + ')';

    // Formats the number with the decimals
    var num = newValue.toFixed(Math.max(0, ~~dl));

    // Returns the formatted number
    return (ds ? num.replace('.', ds) : num).replace(new RegExp(re, 'g'), '$&' + ts);
}

function numberFormat2 (num) {
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}